% % create mask for each facial component
function masks = createMasks(I, landmarks, lm_pos, thicken)

if size(I,3) == 3
    I = im2double(rgb2gray(I));
else
    I = im2double((I));
end

names_groups = fieldnames(lm_pos);
n_groups = numel(names_groups);

masks.raw.total_inner = zeros(size(I));

for ng = 1:n_groups
    x = eval(['landmarks(lm_pos.' names_groups{ng} ', 1);']);
    y = eval(['landmarks(lm_pos.' names_groups{ng} ', 2);']);
    
    % Avoid small discontinuities in ROIs (bwmorph bridge)
    eval(['masks.raw.' names_groups{ng} ' = bwmorph(roipoly(I, x, y), ''bridge'');']);
    % eval(['masks.raw.' names_groups{ng} ' = roipoly(I, x, y);']);
    
    masks.raw.total_inner = masks.raw.total_inner + eval(['masks.raw.' names_groups{ng}]);
end

% % Remove elements smaller than 10 pixels
masks.smooth.total_inner = bwareaopen(masks.raw.total_inner,10);
% masks.smooth.total_inner = masks.raw.total_inner;

% % Thicken component's mask
masks.smooth.total_outter = bwmorph(masks.smooth.total_inner, 'thicken', thicken); % 20 for 640x480px

% % Label masks
masks.smooth.total_outter = bwlabel(masks.smooth.total_outter); 

% % Remove artifacts
se = strel('disk',5);
masks.smooth.total_outter = imclose(masks.smooth.total_outter,se);

% % find border
[gx,gy] = gradient(double(masks.smooth.total_outter));
masks.smooth.total_outter_border = (masks.smooth.total_outter > 0) & ((gx.^2+gy.^2)>0);