% % Set low priority
pri = priority('l');
verbose = true;

% img_folder = 'test-images-cfd/';
% lm_folder = 'test-images-cfd/results/';
% results_folder = 'eyebrows/black_bg_bmp/';

img_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\';
lm_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\landmarks\';
masks_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\masks\';
results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\eyebrows\';
if(~exist(fullfile(results_folder), 'dir')), mkdir(fullfile(results_folder)); end;
if(~exist(fullfile(results_folder, 'masks'), 'dir')), mkdir(fullfile(results_folder, 'masks')); end;

extension = '.bmp';

lm_files = dir([lm_folder '*.lm']);
n_lm_files = numel(lm_files);

for nf=1:n_lm_files
    
    disp(['Getting eyebrows... (' num2str(nf) '/' num2str(n_lm_files) ')']);
    tic
    
    lm_file = lm_files(nf).name;
    img_file = strrep(lm_file, '.lm', extension);
    
    % load landmarks
    landmarks = importdata([lm_folder lm_file]);
    
    % load image
    test_image = im2double(imread([img_folder img_file]));
    h = size(test_image, 1); w = size(test_image, 2);
    
    % load EB masks
    try
        mask_file_LEB = strrep(lm_file, '.lm', '_mask_LEB.bmp');
        maskLEB = imread(fullfile(masks_folder, mask_file_LEB));
        mask_file_REB = strrep(lm_file, '.lm', '_mask_REB.bmp');
        maskREB = imread(fullfile(masks_folder, mask_file_REB));
        
        maskEB = logical(maskLEB) | logical(maskREB);
        
        % apply mask to keep only eyebrows
        [ebi, ebj] = find(maskEB == 1);
        minh = min(ebi);
        maxh = max(ebi);
        minw = min(ebj);
        maxw = max(ebj);
        heb = maxh-minh;
        web = maxw-minw;
        
        % clear eyebrows
        clear eyebrows
        
        % white_mask = imcomplement(mask_eyebrows(minh:maxh,minw:maxw));
        final_mask = maskEB(minh:maxh,minw:maxw);
        eyebrows(:,:,1) = test_image(minh:maxh,minw:maxw,1) .* final_mask; % + white_mask;
        eyebrows(:,:,2) = test_image(minh:maxh,minw:maxw,2) .* final_mask; % + white_mask;
        eyebrows(:,:,3) = test_image(minh:maxh,minw:maxw,3) .* final_mask; % + white_mask;
        
        
        % show eyebrows
        if(verbose)
            close all;
            figure; imshow(eyebrows), title(strrep(lm_file, '_', '-'))
        end
        
        % save eyebrows and masks
        result_img_name = strrep(img_file, extension, '.bmp') ;
        result_mask_name = strrep(img_file, extension, '_mask.bmp') ;
        imwrite(eyebrows, fullfile(results_folder, result_img_name), 'bmp');
        imwrite(final_mask, fullfile(results_folder, 'masks', result_mask_name), 'bmp');
    catch e
        disp(e.getReport());
        fclose(fopen(fullfile(results_folder, strrep(lm_files(nf).name, '.lm', '_couldnt_get_eyebrows.log')), 'w'));
    end
    
    toc
    
end

% % Set back priority
priority(pri);