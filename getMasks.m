function getMasks()

verbose = true;

% % Set low priority
pri = priority('l');

% % CHEHRA Landmarks labels
lm.chehra.LeftEyebrow = 1:5;
lm.chehra.RightEyebrow = 6:10;
lm.chehra.LeftEye = 20:25;
lm.chehra.RightEye = 26:30;
lm.chehra.Nose = [11, 15, 19]; % 11:14;
% lm.chehra.Nostrils = 15:19;
lm.chehra.OutterMouth = 32:43;
% lm.chehra.InnerMouth = 44:49;
% % ---------------------------------------------------------------------->

% img_folder = 'test-images-cfd/';
% lm_folder = 'test-images-cfd/results/';
% results_folder = 'eyebrows/black_bg_bmp/';
% img_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\';
% lm_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\landmarks\';
% results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\helen-test-normalized\masks\';

img_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\';
lm_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\landmarks-corrected\';
results_folder = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\masks-corrected\';

extension = '.jpg';

lm_files = dir([lm_folder '*.lm']);
n_lm_files = numel(lm_files);

IOD = nan(n_lm_files, 1);
thicken = nan(n_lm_files, 1);

for nf=1:n_lm_files
    
    disp(['Extracting eyebrows... (' num2str(nf) '/' num2str(n_lm_files) ')']);
    tic
    
    lm_file = lm_files(nf).name;
    img_file = strrep(lm_file, '.lm', extension);
    
    % % Load landmarks
    landmarks = importdata([lm_folder lm_file]);
    
    % % Load image
    test_image = im2double(imread([img_folder img_file]));
    h = size(test_image, 1); w = size(test_image, 2);
    
    % % Find centroids of Left and Right eyes (first 17 points are contour)
    [~, cxLE, cyLE] = polycenter(landmarks((20:25)+17,1), landmarks((20:25)+17,2));
    [~, cxRE, cyRE] = polycenter(landmarks((26:31)+17,1), landmarks((26:31)+17,2));
      
    % % Eucledian IOD
    IOD(nf) = sqrt((cxLE-cxRE)^2 + (cyLE-cyRE)^2);
    
    % % Create masks for each facial component and draw limits
    thicken(nf) = 0.2*IOD(nf);
    masks = createMasks(test_image, landmarks(18:end,:), lm.chehra, thicken(nf)); %72px thicken
    [ob_i, ob_j] = find(masks.smooth.total_outter_border);
    
    % % Show all detected masks
    if(verbose)
        close all
        figure1 = figure;
        imshow(test_image); title(strrep(img_file, '_', '-'))
        hold on;
        plot(ob_j, ob_i, 'b.', 'MarkerSize', 1)
    end
    
    % % Find mask-feature correspondance
    n_detected_masks = max(masks.smooth.total_outter(:));
    cx = nan(n_detected_masks, 1); % centroids (x coord)
    cy = nan(n_detected_masks, 1); % centroids (y coord)
    fm = zeros(h, w, n_detected_masks); % feature masks
    
    masks_name = {'mask_M', 'mask_N', 'mask_LE', 'mask_RE', 'mask_LEB', 'mask_REB'};
    n_masks = numel(masks_name);
    
    % Find centroids and Area of each region existing in the mask and
    % create a mask for each region individually
    for im=1:n_detected_masks
        [ebi, ebj] = find(masks.smooth.total_outter == im);
        m = zeros(h, w);
        m(sub2ind([h w], ebi, ebj))=1;
        fm(:, :, im) = m;
        b = bwboundaries(m);
        [~, cy(im), cx(im)] = polycenter(b{1,1}(:,1), b{1,1}(:,2));
        
        if(verbose), plot(cx(im), cy(im), '.r', 'MarkerSize', 10); end
    end
    
    % % Decide which mask corresponds with which feature (notice that x and
    % % y in an image start counting from top left corner, x > down, y > right)
    % % Mouth
    try
        ind_M = find(cy == max(cy));
        mask_M = fm(:, :, ind_M); % Mouth mask (downmost)
        euclidean_distance = sqrt((cy(ind_M)-cy).^2+(cx(ind_M)-cx).^2);
        [~, d_to_mouth_y] = sort(euclidean_distance); % EUCLIDEAN distance of all features to the mouth (x, y coords)
        % [~, d_to_mouth_y] = sort(abs(cy(ind_M)-cy)); % distance of all features to the mouth (y coord)
        % % Nose
        ind_N = d_to_mouth_y(2); % 1st is mouth itself, second has to be nose
        mask_N = fm(:, :, ind_N); % Nose mask (closest to mouth in y)
        % % Eyes
        ind_E1 = d_to_mouth_y(3);
        ind_E2 = d_to_mouth_y(4);
        if(cx(ind_E1) < cx(ind_E2))
            ind_LE = ind_E2;
            ind_RE = ind_E1;
        else
            ind_LE = ind_E1;
            ind_RE = ind_E2;
        end
        mask_LE = fm(:, :, ind_LE);
        mask_RE = fm(:, :, ind_RE);
        % % Eyebrows
        [~, d_to_left_eye_x] = sort(abs(cx(ind_LE)-cx)); % distance of all features to the left eye (x coord)
        [~, d_to_right_eye_x] = sort(abs(cx(ind_RE)-cx)); % distance of all features to the right eye (x coord)
        ind_LEB = d_to_left_eye_x(2);
        ind_REB = d_to_right_eye_x(2);
        mask_LEB = fm(:, :, ind_LEB);
        mask_REB = fm(:, :, ind_REB);
        
        % % Save each mask in a bmp file
        if(~exist(results_folder, 'dir')), mkdir(results_folder); end
        for im=1:n_masks
            name = strrep(lm_file, '.lm', ['_' masks_name{im} '.bmp']);
            eval(['imwrite(' masks_name{im} ', fullfile(results_folder, name), ''bmp'');']);
        end
        
        if(~exist(fullfile(results_folder, 'centroids'), 'dir')), mkdir(fullfile(results_folder, 'centroids')); end
        if(verbose), saveas(figure1, fullfile(results_folder, 'centroids', strrep(lm_file, '.lm', '_centroids.bmp')), 'bmp'); end
        if(~exist(fullfile(results_folder, 'mats'), 'dir')), mkdir(fullfile(results_folder, 'mats')); end
        save(fullfile(results_folder, 'mats', strrep(lm_file, '.lm', '_masks.mat')), ...
            'mask_M', 'mask_N', 'mask_LE', 'mask_RE', 'mask_LEB', 'mask_REB');
        
    catch e
        disp(e.getReport());
        if(~exist(fullfile(results_folder, 'errors'), 'dir')), mkdir(fullfile(results_folder, 'errors')); end
        save(fullfile(results_folder, 'errors', strrep(lm_file, '.lm', '_error.mat')), 'e', 'n_detected_masks', 'masks')
    end
    toc
    
end

save(fullfile(results_folder, 'thicken_IOD_values'), 'thicken', 'IOD');

% % Set back priority
priority(pri);